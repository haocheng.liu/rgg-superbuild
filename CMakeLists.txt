cmake_minimum_required(VERSION 3.6.1)

project(rgg-superbuild)

macro (superbuild_setup_variables)
  include(SuperbuildVersionMacros)
  superbuild_set_version_variables(rgg "4.1.1" "rgg-version.cmake")
endmacro ()

function (superbuild_find_projects var)
  set(projects
    boost
    cxx11
    eigen
    freetype
    hdf5
    meshkit
    moab
    netcdf
    png
    qt5
    qttesting
    remus
    rgg
    szip
    vtk
    zeromq
    zlib)

  if (UNIX)
    list(APPEND projects
      gperf
      libxml2)

    if (NOT APPLE)
      list(APPEND projects
        fontconfig)
    endif ()
  endif ()

  set("${var}"
    ${projects}
    PARENT_SCOPE)
endfunction ()

function (superbuild_sanity_check)
  include(qt.functions)

  superbuild_qt_sanity_check()

  if (NOT rgg_enabled)
    message(FATAL_ERROR "RGG is disabled...")
  endif ()
endfunction ()

function (superbuild_add_packaging)
  if (WIN32)
    set(generators
      ZIP)
  elseif (APPLE)
    set(generators
      DragNDrop)
  endif ()
  list(GET generators 0 default_generator)

  if (USE_SYSTEM_qt5)
    list(APPEND superbuild_export_variables
      Qt5_DIR)
  endif ()

  if (paraview_enabled)
    set(PARAVIEW_PACKAGE_FILE_NAME ""
      CACHE STRING "If set, overrides the generated filename for the package")
    list(APPEND superbuild_export_variables
      PARAVIEW_PACKAGE_FILE_NAME)
  endif ()

  set(default_package)

  foreach (generator IN LISTS generators)
    if (rgg_enabled)
      if (NOT default_package)
        set(default_package rgg)
      endif ()
      superbuild_add_extra_package_test(rgg "${generator}"
        LABELS  "RGG"
        TIMEOUT 6400)
    endif ()
  endforeach ()

  if (default_package)
    superbuild_enable_install_target("${default_package}/${default_generator}")
  endif ()
endfunction ()

function (superbuild_add_tests)
endfunction ()

set(qt5_skip_modules
  qtconnectivity
  qtgamepad
  qtlocation
  qtmultimedia
  qtsensors
  qtserialport
  qtwayland
  qtwebchannel
  qtwebengine
  qtwebsockets)

set(boost_libraries
  atomic
  chrono
  date_time
  filesystem
  system
  thread)
set(boost_extra_options
  "-sNO_BZIP2=1")

list(APPEND superbuild_version_files
  "${CMAKE_CURRENT_LIST_DIR}/versions.cmake")
list(APPEND superbuild_project_roots
  "${CMAKE_CURRENT_LIST_DIR}/projects")

# set the default arguments used for "git clone"
set(_git_clone_arguments_default --progress)

# set the default for rgg to be enabled for this project
set(_superbuild_default_rgg ON)

# set the default for qt5 to be 5.8
set(_superbuild_qt5_default_selection "5.8")

if (NOT EXISTS "${CMAKE_CURRENT_LIST_DIR}/superbuild/CMakeLists.txt")
  message(FATAL_ERROR "It appears as though the superbuild infrastructure "
                      "is missing; did you forget to do `git submodule init` "
                      "and `git submodule update`?")
endif ()

add_subdirectory(superbuild)
