superbuild_add_project(rgg
  DEPENDS cxx11 remus qt5 vtk moab qttesting boost
  CMAKE_ARGS
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications
    -DVTK_MOAB_SUPPORT:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF)
