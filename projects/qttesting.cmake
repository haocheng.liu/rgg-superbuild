superbuild_add_project(qttesting
  DEPENDS cxx11 qt5
  CMAKE_ARGS
    -DBUILD_TESTING:BOOL=OFF
    -DQtTesting_QT_VERSION:STRING=5)
