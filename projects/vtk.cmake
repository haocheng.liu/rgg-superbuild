superbuild_add_project(vtk
  DEPENDS cxx11 qt5
  CMAKE_ARGS
    -DBUILD_TESTING:BOOL=OFF
    -DVTK_QT_VERSION:STRING=5
    -DModule_vtkGUISupportQt:BOOL=ON
    -DModule_vtkGUISupportQtOpenGL:BOOL=ON
    -DModule_vtkRenderingQt:BOOL=ON
    -DModule_vtkViewsQt:BOOL=ON
    -DModule_vtkTestingCore:BOOL=ON
    -DModule_vtkTestingRendering:BOOL=ON
    -DModule_vtkTestingGenericBridge:BOOL=ON
    -DModule_vtkTestingIOSQL:BOOL=ON
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2
    -DVTK_REQUIRED_OBJCXX_FLAGS:STRING="")
