superbuild_add_project(cgm
  CMAKE_ARGS
    -DCGM_USE_CUBIT:BOOL=OFF
    -DCGM_OCC:BOOL=OFF
    -DUSE_MPI:BOOL=OFF)

superbuild_add_extra_cmake_args(
  -DCGM_DIR:PATH=<INSTALL_DIR>/lib/cmake/CGM)
