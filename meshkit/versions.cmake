superbuild_set_revision(cgm
  # "https://bitbucket.org/fathomteam/cgm.git"
  # "https://bitbucket.org/mathstuf/cgm.git"
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/cgm-3191a7219e16491ebe098159598ddfe7f039bffc.tar.bz2"
  URL_MD5 bf280f3bc5970c98960da9a66da2ab94)

superbuild_set_revision(meshkit
  GIT_REPOSITORY  "https://haocheng_liu@bitbucket.org/haocheng_liu/meshkit.git"
  GIT_TAG         osx-10-dot-13-patch)
